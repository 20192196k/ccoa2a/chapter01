/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.aliquel.p22043001;

/**
 *
 * @author Alexander Lique <alexander.lique.l@uni.pe>
 */
public class Main {
    public static void main(String [] args){
        System.out.println("loop statement");
        //int i;
        for(int i=0; i<10; i++){
            System.out.println(i);
        }
        {//contexto xd 
            int j = 0;
            System.out.println(++j);
        }
        for(int i=9; i>=0; i--){
            System.out.println(i);
        }
         //Matriz 5 x 3
         int f = 5;
         int c = 3;
         for (int i=0; i<f; i++){
             for(int j=0; j<c; j++){
                System.out.print("("+i+"," + j+")");
             }
             System.out.print("\n");
         }
         int i=0;
         while(i<10){
             i++;
             System.out.println(i);
         }
         i=10;
         while(0<i){
             System.out.println(i);
             i--;
         }
         i=0;       
         while (i<f){
             int j=0;
             while (j<c){
                 System.out.print("("+i+"," + j+")");
                 j++;
             }
             System.out.print("\n");
             i++;
         }
         
         i = 0;
         while(i<0){
             System.out.print("ok");
         }
         do{
             System.out.print("ok");
         }while(i<0);
         System.out.print("------");
         i=10;
         do{
             System.out.print(i);
             System.out.print("\n");
             i--;
         }while(i>=0);
         i=1;
         do{
             int j=1;
             do{
                 System.out.print("("+i+"," + j+")");
                 j++;
             }while(j<=c);
             System.out.print("\n");
             i++;
         }while(i<=f);
         
          int p=10;
          
          for(int j=2; j<p ; j++){
              if (p%i==0){
                  System.out.println("tiene un primo");
                  break;
              }
          }
        
        /*for(int z=10; z<90;p++){
          int contador = 0;
          for(int j=1; j<z+1 ; j++){
              if (p%i==0){
                  contador++;
                  break;
              }
             }
          if(contador==2){
              System.out.println(z);
            }
          }*/
    }
}
