/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.aliquel.pc22042801;

/**
 *
 * @author Alexander Lique <alexander.lique.l@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Hello world");
        System.out.println("longitud del array "+args.length);
        for(int i=0; i<args.length; i++){
            System.out.println("longitud del array "+args[i]);
        }
        int a = Integer.valueOf(args[0]);
        int b = Integer.valueOf(args[1]);
        int c = Integer.valueOf(args[2]);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(args[0]+ args[1]);
        System.out.println(a+ b);
        System.out.println("Suma "+(a+b));
        System.out.println("Suma "+String.valueOf(a+b));
        System.out.println(a + b + " Suma ");
        System.out.println(a+ b);
        System.out.println(a-b);
        System.out.println(a*b);
        System.out.println(a/b);
        System.out.println(a%b);
        System.out.println(a);
        System.out.println("unary operations");
        System.out.println(a++);
        System.out.println(a);
        System.out.println(a--);
        System.out.println(a);
        System.out.println("relational operators");
        System.out.println(a==b);
        System.out.println(a != b);
        System.out.println(a > b);
        System.out.println(a >= b);
        System.out.println(a < b);
        System.out.println(a <= b);
        System.out.println(a<b && b<c);
        System.out.println("bitwise");
        System.out.println(4&7);
        System.out.println("operadores logicos");
        boolean v = true;
        boolean f = false;
        System.out.println(f && v);
        System.out.println(!f && v);
        System.out.println(!f || v);
        int d = 4;
        System.out.println(d);
        d+=8;
        System.out.println(d);
        d-=2;
        System.out.println(d);
        System.out.println("conditional operators");
        int result = (a!=b) ? 1 : 2;
        result = (--a == b) ? 1 : 2; 
        System.out.println(result);
    }
}
