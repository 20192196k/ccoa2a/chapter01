/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.aliquel.p2205501;

import java.util.Arrays;

/**
 *
 * @author Alexander Lique <alexander.lique.l@uni.pe>
 */
public class Main {
    public static void main(String [] args){
    System.out.println("string");
    String string = "viva la IA";
    System.out.println("lognitud: "+ string.length());
    System.out.println("1sr caracter "+string.charAt(0));
    System.out.println("ultimo caracter "+string.charAt(string.length()-1));
    System.out.println("conversion");
    System.out.println(string.toUpperCase());
    System.out.println("Splits");
    String [] parts =  string.split(" ");
    System.out.println("part size "+  parts.length);
    for(int i=0; i<parts.length; i++){
        System.out.println(parts[i]);
    }
    System.out.println("parts"+ Arrays.toString(parts));
    System.out.println("trim");
    String t = string.trim();
    System.out.println("len "+ t.length());
    }
}
